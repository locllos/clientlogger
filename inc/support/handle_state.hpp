#pragma once

enum class HandleState {
  AcceptError,
  WriteError,

  AddNewClient,

  ServerClose,
  ClientClose,

  OK,
};
