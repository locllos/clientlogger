const char* kHelpMessage = "Usage:\n\
Required arguments:\n\
    * IP (Internet Protocol)\n\
    * port\n\
    * [client, host]\n\
If server was chosen next argument:\n\
    * database path\n \
To exit type:\n \
    * \\close\n";

const char* kClientArgument = "client";
const char* kHosterArgument = "host";
