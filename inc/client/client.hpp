#include <core/epoll.hpp>
#include <core/socket.hpp>
#include <support/handle_state.hpp>

class Client {
 public:
  Client(const char* ip, const char* port);

  void Run();

  ~Client();

 private:

 EventPoll epoll_;
 Socket socket_;

 char* buffer_;

 private:

  HandleState HandleSTDIO();

 private:

  static const char* kClientClose;
  static const int kBufferSize = 513;
  static const int kMaxEvents = 2;

  bool is_closed_;
};