#include <core/epoll.hpp>
#include <core/socket.hpp>
#include <core/tiny_database.hpp>
#include <support/handle_state.hpp>
#include <unordered_map>

class Server {
 public:

  Server(const char* ip, const char* port);

  void OpenDatabase(const char* filename);

  void Run();

  ~Server();

 private:
 
  HandleState HandleSTDIO();
  HandleState HandleNewClient();
  HandleState HandleClient(FileDescroptor client_fd);
  HandleState HandleClientClose(FileDescroptor client_fd);

 private:

  // TODO: ClientManager
  int clients_count_;
  std::unordered_map<FileDescroptor, ClientID> client_map_;

  bool is_closed_;

  // TODO:
  // unite Socket and Epoll to another class?
  // Seems like good idea. Rename Server to Hoster and make base class Server
  // with virtual function Run(). Hoster and Client will inherit Server
  // unfortunately, I still cannot code perfect straightaway
  // if i will have time, i refactor it
  Socket socket_;
  EventPoll epoll_;
  TinyDatabase database_;

  char* buffer_;

 private:

  static const char* kServerClose;
  static const int kBufferSize = 513;
  static const int kClientConnectionsMax = 5;
};