#pragma once

#include <stdio.h>
#include <string>

// very simple implemention for now
// only recording without extracting and buffering
class TinyDatabase {
 public:

  TinyDatabase();

  TinyDatabase(const char* filename);

  void Open(const char* filename);
  void Close();

  void Record(char* content, int id = 0);
  void Record(const char* content, int id = 0);

  // char* ExtractContentRaw(int id);
  // std::string ExtractContentSTD(int id);

  // void Upload();
  // void Unload();

  ~TinyDatabase();

 private:
  FILE* file_;

  // char* buffer
};