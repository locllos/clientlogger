#pragma once

#include <arpa/inet.h>
#include <fcntl.h>

#include "../support/usings.hpp"

// tiny wrapper over low-level socket with only necessary funcions
void SetNonBlockStream(FileDescroptor fd);

class Socket {
 public:

  Socket(FileDescroptor fd);
  Socket(Domain domain, Type type, Protocol protocol = 0);

  void LinkEndPoint(const char* ip, const char* port);
  
  FileDescroptor Accept();

  void Bind();
  void Listen(int connections_count);
  void Connect();

  void SetNonBlockStream();

  void Shutdown(int mode = O_RDWR);
  void Close();

  FileDescroptor GetFD() const;

  // socket must be own its file descriptor
  ~Socket();

 private:

  FileDescroptor fd_;
  SocketAddress socket_address_;
};