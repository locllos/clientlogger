#pragma once

#include <sys/epoll.h>
#include <vector>
#include <support/usings.hpp>

using Event = struct epoll_event;

// tiny wrapper over low-level epoll
class EventPoll {
 public:

  EventPoll(int max_events, int time_out = -1);

  // only "in"
  void AddHandledEvent(FileDescroptor fd);

  // zero copies
  Event* Wait(int* event_count); // argument is pointer here is to show that argument changes
  // one copies
  std::vector<Event> Wait();

  ~EventPoll();
 private:

  FileDescroptor fd_;

  // FIXME: unique_ptr is better??? 
  Event* events_;

  int max_events_;
  int time_out_;
};