cmake_minimum_required(VERSION 3.0.0)
project(ServerLogger VERSION 0.1.0)

include_directories(ServerLogger inc)

add_executable(ServerLogger src/main.cpp)

add_subdirectory(src)

target_link_libraries(ServerLogger Core Client Server)