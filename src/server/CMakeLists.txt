add_library(Server)

target_include_directories(Server PUBLIC ServerLogger_SOURCE_DIR/inc)

target_sources(Server
               PRIVATE 
               server.cpp)

target_link_libraries(Server
                      PUBLIC
                      Core
                      Client)
