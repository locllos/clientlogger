#include <server/server.hpp>
#include <unistd.h>
#include <string.h>

const char* Server::kServerClose = "\\close";
const int kServerCloseLength = 6;

int CreateClientID() {
  static ClientID client_id = 1;

  return client_id++;
}

Server::Server(const char* ip, const char* port) 
  : clients_count_{0},
    is_closed_{false},
    socket_{AF_INET, SOCK_STREAM},
    epoll_{2 * kClientConnectionsMax + 1},
    buffer_{new char[kBufferSize]} {
  
  socket_.LinkEndPoint(ip, port);
  socket_.Bind();
  socket_.Listen(kClientConnectionsMax + 1); // +1 is STDIN

  epoll_.AddHandledEvent(STDIN_FILENO);
  epoll_.AddHandledEvent(socket_.GetFD());
}

void Server::OpenDatabase(const char* filename) {
  database_.Open(filename);

  database_.Record("[Start logging]");
}

HandleState Server::HandleSTDIO() {
  int bytes_count = read(STDIN_FILENO, buffer_, kBufferSize - 1);
    
  buffer_[bytes_count - 1] = '\0';

  if (strncmp(buffer_, kServerClose, kServerCloseLength) == 0) {
    is_closed_ = true;
    return HandleState::ServerClose;
  }
  printf("Unknown server command.\n");

  return HandleState::OK;
}

HandleState Server::HandleNewClient() {
  FileDescroptor client_fd = socket_.Accept();

  if (client_fd < 0) {
    perror("Unable accept to client.");
    return HandleState::AcceptError;
  }
  ClientID client_id = CreateClientID();
  client_map_.emplace(client_fd, client_id);

  printf("Got connection with id: %zu.\n", client_id);

  SetNonBlockStream(client_fd);
  epoll_.AddHandledEvent(client_fd);

  ++clients_count_;

  return HandleState::OK;
}

HandleState Server::HandleClientClose(FileDescroptor client_fd) {
  int bytes_count = read(client_fd, buffer_, kBufferSize - 1); // -1, bc bytes_count should be < KBufferSize

  if (bytes_count < 1) {
    printf("Connection with client[id = %zu] was closed.\n", client_map_[client_fd]);

    Socket client{client_fd};

    client.Shutdown();

    --clients_count_;
    client_map_.erase(client_fd);

    return HandleState::ClientClose;
  }
  buffer_[bytes_count - 1] = '\0';

  return HandleState::OK;
}

HandleState Server::HandleClient(FileDescroptor client_fd) {

  if (HandleClientClose(client_fd) == HandleState::ClientClose) {
    return HandleState::ClientClose;
  }
  printf("Got client[id = %zu] data.\n", client_map_[client_fd]);

  // record content which was gotten from HandleClientClose
  database_.Record(buffer_, client_map_[client_fd]);

  int bytes_count = 0;
  while ((bytes_count = read(client_fd, buffer_, kBufferSize - 1)) > 0) {
    buffer_[bytes_count - 1] = '\0';
    database_.Record(buffer_);
  }

  return HandleState::OK;
}

void Server::Run() {
  printf("Server was started.\n");
  HandleState state = HandleState::OK;

  while (!is_closed_) {
    auto events = epoll_.Wait(); 

    for (auto& event : events) {
      // Callback is better but time is limited and i am overflowing another ideas
      if (event.data.fd == STDIN_FILENO) {
        state = HandleSTDIO();
      }
      else if (event.data.fd == socket_.GetFD()) {
        printf("New client caught.\n");

        if (clients_count_ > kClientConnectionsMax) {
          continue;
        }
        state = HandleNewClient();
      }
      else {
        printf("Default client event caught.\n");

        state = HandleClient(event.data.fd);
      }
      
      if (state == HandleState::ServerClose) {
        break;
      }
    }
  }
  printf("Server was closed.\n");
}

Server::~Server() {
  delete[] buffer_;
}