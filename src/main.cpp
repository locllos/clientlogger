#include <server/server.hpp>
#include <client/client.hpp>
#include <support/messages.hpp>

#include <string.h>

void HelpMessage() {
  printf("%s", kHelpMessage);
}

int main(int argv, const char* argc[]) {
  
  if (argv < 4) {
    HelpMessage();
    return 0;
  }

  if (strcmp(argc[3], kClientArgument) == 0) {
    Client client{argc[1], argc[2]};

    client.Run();
  }
  else if (strcmp(argc[3], kHosterArgument) == 0) {
    if (argv != 5) {
      HelpMessage();
      return 0;
    }
    Server server{argc[1], argc[2]};

    server.OpenDatabase(argc[4]);

    server.Run();
  }
  else {
    HelpMessage();
  }

  return 0;
}
