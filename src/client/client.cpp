#include <client/client.hpp>
#include <unistd.h>
#include <stdio.h>
#include <string.h>

const char* Client::kClientClose = "\\close";
const int kClientCloseLength = 6;

Client::Client(const char* ip, const char* port) 
  : epoll_{kMaxEvents},
    socket_{AF_INET, SOCK_STREAM},
    buffer_{new char[kBufferSize]} {
  
  socket_.LinkEndPoint(ip, port);

  socket_.Connect();
  epoll_.AddHandledEvent(STDIN_FILENO);
}

HandleState Client::HandleSTDIO() {
  int bytes_count = read(STDIN_FILENO, buffer_, kBufferSize - 1);
  buffer_[bytes_count] = '\0';  

  if (strncmp(buffer_, kClientClose, kClientCloseLength) == 0) {
      is_closed_ = true;
      return HandleState::ClientClose;
  }

  if (write(socket_.GetFD(), buffer_, bytes_count) < 0) {
    perror("Unable to write to server");

    return HandleState::WriteError;
  }
  printf("Message was sent successfully.\n");

  return HandleState::OK;
}

void Client::Run() {
  printf("Client was started.\n");

  HandleState state = HandleState::OK;

  while (!is_closed_) {
    auto events = epoll_.Wait(); 

    for (auto& event : events) {
      if (event.data.fd == STDIN_FILENO) {
        state = HandleSTDIO();
      }
      else {
        printf("Unknown event[fd = %d].\n", event.data.fd);
      }

      if (state == HandleState::ClientClose) {
        break;
      }
    }
  }
  printf("Client was closed.\n");
}

Client::~Client() {
  delete[] buffer_;
}