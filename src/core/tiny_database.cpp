#include <core/tiny_database.hpp>

TinyDatabase::TinyDatabase() 
  : file_{nullptr} {
}

TinyDatabase::TinyDatabase(const char* filename) {
  file_ = fopen(filename, "w");

  if (file_ == nullptr) {
    perror("Unable to create file");
  }
}

void TinyDatabase::Open(const char* filename) {
  if (file_ != nullptr) {
    perror("Database is already opened.");
    return;
  }

  file_ = fopen(filename, "w");

  if (file_ == nullptr) {
    perror("Unable to create file");
  }
}

void TinyDatabase::Close() {
  fclose(file_);

  file_ = nullptr;
}

void TinyDatabase::Record(char* content, int id) {

  if (file_ == nullptr) {
    perror("Database is not opened.");
    return;
  }

  if (id < 1) {
    // continue writing previous 
    fprintf(file_, "%s", content);
  }
  else {
    fprintf(file_, "\n%d - %s", id, content);
  }
  printf("content: %s\n", content);
}

void TinyDatabase::Record(const char* content, int id) {

  if (file_ == nullptr) {
    perror("Database is not opened.");
    return;
  }

  if (id < 1) {
    // continue writing previous 
    fprintf(file_, "%s", content);
  }
  else {
    fprintf(file_, "\n%d - %s", id, content);
  }
}

TinyDatabase::~TinyDatabase() {
  fclose(file_);
}