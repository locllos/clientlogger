#include <sys/socket.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>

#include <core/socket.hpp>

void SetNonBlockStream(FileDescroptor fd) {
  fcntl(fd, F_SETFL, fcntl(fd, F_GETFL, 0) | O_NONBLOCK);
}


Socket::Socket(FileDescroptor fd) 
  : fd_{fd} {
}

Socket::Socket(Domain domain, Type type, Protocol protocol) {

  fd_ = socket(domain, type, protocol);

  if (fd_ < 0) {
    perror("Error while creating socket file descriptor");
  }
}

void Socket::LinkEndPoint(const char* IP, const char* port) {

  socket_address_.sin_family = AF_INET;
  inet_aton(IP, &socket_address_.sin_addr);
  socket_address_.sin_port = htons((uint16_t)strtol(port, NULL, 10));
}

FileDescroptor Socket::Accept() {
  FileDescroptor accepted_fd = accept(fd_, NULL, NULL);

  if (accepted_fd < 0) {
    perror("Unable to accept");
  }

  return accepted_fd;
}

void Socket::Bind() {
  
  if (bind(fd_, (struct sockaddr*)(&socket_address_), sizeof(socket_address_)) < 0) {
    perror("Unable to bind server");
  }
}

void Socket::Listen(int connections_count) {
  if (listen(fd_, connections_count) < 0) {
    perror("Unable to listen socket");
  }
}

void Socket::Connect() {
 
  if (connect(fd_, (struct sockaddr*)(&socket_address_), sizeof(socket_address_)) < 0) {
    perror("Unable to connect to server");
  }
  else {
    printf("Connection to server was successful.\n");
  }
}

void Socket::SetNonBlockStream() {
  fcntl(fd_, F_SETFL, fcntl(fd_, F_GETFL, 0) | O_NONBLOCK);
}

FileDescroptor Socket::GetFD() const {
  return fd_;
}

void Socket::Shutdown(int mode) {
  shutdown(fd_, mode);
}

void Socket::Close() {
  close(fd_);
}

Socket::~Socket() {
  close(fd_);
}