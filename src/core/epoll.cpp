#include <core/epoll.hpp>
#include <unistd.h>
#include <stdio.h>

EventPoll::EventPoll(int max_events, int time_out) 
  : fd_{epoll_create1(0)},
    events_{new Event[max_events]},
    max_events_{max_events},
    time_out_{time_out} {
}

void EventPoll::AddHandledEvent(FileDescroptor new_fd) {
  struct epoll_event initialize_event = {};

  initialize_event.events = EPOLLIN; 
  initialize_event.data.fd = new_fd;
  if (epoll_ctl(fd_, EPOLL_CTL_ADD, new_fd, &initialize_event) < 0) {
    perror("Unable to add new epoll event");
  };
}

std::vector<Event> EventPoll::Wait() {

  int events_count = epoll_wait(fd_, events_, max_events_, time_out_);

  // there is one copy which can be avoided
  return std::vector<Event>(events_, events_ + events_count);
}

Event* EventPoll::Wait(int* event_count) {
  *event_count = epoll_wait(fd_, events_, max_events_, time_out_);

  return events_;
}

EventPoll::~EventPoll() {
  close(fd_);

  delete[] events_;
}
